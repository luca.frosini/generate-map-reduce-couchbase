function(doc,meta) {
	if(doc.usageRecordType == "JobUsageRecord" || doc.recordType == "JobUsageRecord"){
		if((doc.duration && isNaN(Number(doc.duration))) || (doc.operationCount && isNaN(Number(doc.operationCount)))){
			emit([0, "Invalid Value", doc.id], doc);
			return;
		}
 		var data = {};
 		data.jobName = doc.jobName;
 		data.operationCount = doc.operationCount ? Number(doc.operationCount) : 1;
 		data.serviceClass = doc.serviceClass;
 		data.creationTime = doc.creationTime;
 		
 		var callerQualifier=doc.callerQualifier ? (doc.callerQualifier) : "UNKNOWN";
 		data.callerQualifier=callerQualifier;
 		
 		data.recordType = doc.recordType;
 		data.consumerId = doc.consumerId;
 		data.aggregated = doc.aggregated;
 		data.serviceName = doc.serviceName;
 		data.duration = doc.duration ? Number(doc.duration) : 1;
 		data.maxInvocationTime = doc.maxInvocationTime ? doc.maxInvocationTime : data.duration;
 		data.scope = doc.scope;
 		data.host = doc.host;
 		data.startTime = doc.startTime;
 		data.id = doc.id;
 		data.endTime = doc.endTime;
		data.minInvocationTime = doc.minInvocationTime ? doc.minInvocationTime : data.duration;
		data.operationResult = doc.operationResult;
		
		data._rev = doc._rev;
		data._id = doc._id;

		var timestamp = Number(doc.creationTime);
        var date = new Date(timestamp);
		var dataKey = [];
		dataKey.push(date.getUTCFullYear());
		dataKey.push(date.getUTCMonth()+1);
		dataKey.push(date.getUTCDate());
		dataKey.push(date.getUTCHours());
		dataKey.push(date.getUTCMinutes());		
		emit(dataKey, data);
	}
}
 

