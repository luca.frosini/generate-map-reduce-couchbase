function(doc,meta) {
	if(doc.usageRecordType == "ServiceUsageRecord" || doc.recordType == "ServiceUsageRecord"){
		if((doc.duration && isNaN(Number(doc.duration))) || (doc.operationCount && isNaN(Number(doc.operationCount)))){
			emit([0, "Invalid Value", doc.id], doc);
			return;
		}
 		var data = {};
		data.host = doc.host;
		data.scope = doc.scope;
		data.callerHost = doc.callerHost;
		data.operationCount = doc.operationCount ? Number(doc.operationCount) : 1;
		data.minInvocationTime = doc.minInvocationTime ? doc.minInvocationTime : data.duration;
		data.maxInvocationTime = doc.maxInvocationTime ? doc.maxInvocationTime : data.duration;
		data.endTime = doc.endTime;
		data.serviceClass = doc.serviceClass;
		data.serviceName = doc.serviceName;
		data.consumerId = doc.consumerId;
		data._rev = doc._rev;
		data.startTime = doc.startTime;
		data.id = doc.id;
		data.duration = doc.duration ? Number(doc.duration) : 1;
		data._id = doc._id;
		data.calledMethod = doc.calledMethod;
		data.operationResult = doc.operationResult;
		data.aggregated = doc.aggregated;
		data.creationTime = doc.creationTime;
		data.recordType = doc.recordType;
		data.usageRecordType=doc.usageRecordType;
		var callerQualifier=doc.callerQualifier ? (doc.callerQualifier) : "UNKNOWN";
		data.callerQualifier=callerQualifier;          
		var timestamp = Number(doc.creationTime);
        	var date = new Date(timestamp);
		var dataKey = [];
		dataKey.push(date.getUTCFullYear());
		dataKey.push(date.getUTCMonth()+1);
		dataKey.push(date.getUTCDate());
		dataKey.push(date.getUTCHours());
		dataKey.push(date.getUTCMinutes());		
		emit(dataKey, data);
	}
}
 

