function(doc,meta) { 
	if(doc.usageRecordType == "ServiceUsageRecord" || doc.recordType == "ServiceUsageRecord"){ 
		if((doc.operationCount && isNaN(Number(doc.operationCount))) || (doc.duration && isNaN(Number(doc.duration)))){ 
			emit([0, "Invalid Value", doc.id], doc); 
			return; 
		} 
        	var data = {}; 
		data[doc.serviceName] = doc.operationCount ? Number(doc.operationCount) : 1; 
		var propertiesKey = []; 
		var timestamp=Number(doc.creationTime); 
		var date = new Date(timestamp); 
		var dataKey = [];  
		dataKey.push(date.getUTCFullYear());  
		dataKey.push(date.getUTCMonth()+1);  
		dataKey.push(date.getUTCDate());  
		dataKey.push(date.getUTCHours());  
		var finalKey = propertiesKey.concat(dataKey); 
		finalKey.unshift(doc.scope); 
		emit(finalKey, data); 
	} 
}
