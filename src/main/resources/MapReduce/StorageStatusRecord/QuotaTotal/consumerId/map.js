function(doc,meta) {
	if(doc.usageRecordType == "StorageStatusRecord" || doc.recordType == "StorageStatusRecord"){
          if(doc.providerId == "MongoDb"){
            if((doc.dataVolume && isNaN(Number(doc.dataVolume))) || (doc.dataCount && isNaN(Number(doc.dataCount)))|| (doc.operationCount && isNaN(Number(doc.operationCount))))
                  {
                          emit([0, "Invalid Value", doc.id], doc);
                          return; 
                  }
                  var data = {};
                  data.dataVolume = doc.dataVolume ? Number(doc.dataVolume) : 0;
                  data.dataCount = doc.dataCount ? Number(doc.dataCount) : 0;
                  data.dataCreationTime = doc.creationTime ? Number(doc.creationTime) : Number(doc.startTime);
                  var timestamp = Number(doc.creationTime);
                  var date = new Date(timestamp);
                  var dataKey = [];
                  dataKey.push(date.getUTCFullYear());
                  dataKey.push(date.getUTCMonth()+1);
                  dataKey.push(date.getUTCDate());
                  dataKey.push(date.getUTCHours());
                  dataKey.push(date.getUTCMinutes());
                  dataKey.unshift(doc.consumerId);	
                  dataKey.unshift(doc.scope);
                  emit(dataKey, data);
          }
	}
}
