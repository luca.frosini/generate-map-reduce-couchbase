function(keys, values, rereduce){
	ret = {}; 
	ret = values[0];	
	for(i=1; i<values.length; i++){ 
		var value = values[i]; 
		var fields = Object.keys(value); 
          
 		for(j=0; j<fields.length; j++){ 
			var fieldKey = fields[j];
	          	if(ret[fieldKey]){
                          	if (ret[fieldKey][1]<=Number(value[fieldKey][1])){
					ret[fieldKey]=value[fieldKey];
				}
			}else{
				ret[fieldKey] = value[fieldKey];
			}
        			          
		} 
	} 
  return ret;
}
