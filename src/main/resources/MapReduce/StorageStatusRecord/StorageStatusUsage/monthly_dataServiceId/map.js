function(doc,meta) { 
	if(doc.usageRecordType == "StorageStatusRecord" || doc.recordType == "StorageStatusRecord"){
		if((doc.dataVolume && isNaN(Number(doc.dataVolume))) || (doc.dataCount && isNaN(Number(doc.dataCount)))|| (doc.operationCount && isNaN(Number(doc.operationCount))))
                {
			emit([0, "Invalid Value", doc.id], doc);
			return;
		}
		var data = {};
	        data[doc.providerId+"-"+doc.dataServiceId] = [];
		data[doc.providerId+"-"+doc.dataServiceId].push(doc.dataVolume);
	      	data[doc.providerId+"-"+doc.dataServiceId].push(doc.creationTime);
         	var timestamp = Number(doc.creationTime);
		var date = new Date(timestamp);
		var dataKey = [];
	        dataKey.push(doc.dataServiceId);	
		dataKey.push(date.getUTCFullYear());
		dataKey.push(date.getUTCMonth()+1);		
		dataKey.push(doc.consumerId);	
		
		dataKey.unshift(doc.scope);
		emit(dataKey, data);
	}
}
