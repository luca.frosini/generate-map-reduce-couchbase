function(doc,meta) {
	if(doc.usageRecordType == "ServiceUsageRecord" || doc.recordType == "ServiceUsageRecord"){
		if((doc.operationCount && isNaN(Number(doc.operationCount))) || (doc.duration && isNaN(Number(doc.duration)))){
			emit([0, "Invalid Value", doc.id], doc);
			return;
		}

		var data = {};
		data.operationCount = doc.operationCount ? Number(doc.operationCount) : 1;
		data.duration = doc.duration ? Number(doc.duration) : 1;
		data.maxInvocationTime = doc.maxInvocationTime ? Number(doc.maxInvocationTime) : data.duration;
		data.minInvocationTime = doc.minInvocationTime ? Number(doc.minInvocationTime) : data.duration;
		
		var propertiesKey = [];
		propertiesKey.push(doc.host);
		propertiesKey.push(doc.operationResult);
		propertiesKey.push(doc.serviceName);
		
	
		var timestamp=Number(doc.creationTime);
		var date = new Date(timestamp);
		var dataKey = [];
		dataKey.push(date.getUTCFullYear());
		dataKey.push(date.getUTCMonth()+1);
		dataKey.push(date.getUTCDate());
		dataKey.push(date.getUTCHours());
		dataKey.push(date.getUTCMinutes());
//		dataKey.push(date.getUTCSeconds());
//		dataKey.push(date.getUTCMilliseconds());
		var finalKey = propertiesKey.concat(dataKey);

		finalKey.unshift(doc.scope);
		
		emit(finalKey, data);
	}
}
