function(doc,meta) {
	if(doc.usageRecordType == "StorageUsageRecord" || doc.recordType == "StorageUsageRecord"){
		if((doc.operationCount && isNaN(Number(doc.operationCount))) || (doc.dataVolume && isNaN(Number(doc.dataVolume)))){
			emit([0, "Invalid Value", doc.id], doc);
			return;
		}
		var data = {};
		data.dataVolume = doc.dataVolume ? Number(doc.dataVolume) : 0;
		data.operationCount = doc.operationCount ? Number(doc.operationCount) : 1;
		
		var propertiesKey = [];
		propertiesKey.push(doc.consumerId);
		propertiesKey.push(doc.operationType);
		propertiesKey.push(doc.resourceOwner);
		propertiesKey.push(doc.resourceScope);
		
		var timestamp=Number(doc.creationTime);
		var date = new Date(timestamp);
		var dataKey = [];
		dataKey.push(date.getUTCFullYear());
		dataKey.push(date.getUTCMonth()+1);
		dataKey.push(date.getUTCDate());
		dataKey.push(date.getUTCHours());
		dataKey.push(date.getUTCMinutes());
//		dataKey.push(date.getUTCSeconds());
//		dataKey.push(date.getUTCMilliseconds());
		
		var finalKey = propertiesKey.concat(dataKey);

		finalKey.unshift(doc.scope);
		
		emit(finalKey, data);
	}
}
