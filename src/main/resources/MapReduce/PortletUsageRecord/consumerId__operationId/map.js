function(doc,meta) {
	if(doc.usageRecordType == "PortletUsageRecord" || doc.recordType == "PortletUsageRecord"){
		if((doc.duration && isNaN(Number(doc.duration))) || (doc.operationCount && isNaN(Number(doc.operationCount)))){
			emit([0, "Invalid Value", doc.id], doc);
			return;
		}

		var data = {};
		data.operationCount = doc.operationCount ? Number(doc.operationCount) : 1;
		
		var propertiesKey = [];
		propertiesKey.push(doc.consumerId);
		propertiesKey.push(doc.operationId);
		
		var timestamp=Number(doc.creationTime);
		var date = new Date(timestamp);
		var dataKey = [];
		dataKey.push(date.getUTCFullYear());
		dataKey.push(date.getUTCMonth()+1);
		dataKey.push(date.getUTCDate());
		dataKey.push(date.getUTCHours());
		dataKey.push(date.getUTCMinutes());
//		dataKey.push(date.getUTCSeconds());
//		dataKey.push(date.getUTCMilliseconds());
		var finalKey = propertiesKey.concat(dataKey);

		finalKey.unshift(doc.scope);
		
		emit(finalKey, data);
	}
}

