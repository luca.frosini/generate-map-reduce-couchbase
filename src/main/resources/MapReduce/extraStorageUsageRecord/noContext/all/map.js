function(doc,meta) {
	if(doc.usageRecordType == "StorageUsageRecord" || doc.recordType == "StorageUsageRecord"){
		if((doc.dataVolume && isNaN(Number(doc.dataVolume))) || (doc.operationCount && isNaN(Number(doc.operationCount)))){
			emit([0, "Invalid Value", doc.id], doc);
			return;
		}
		var data = {};
		data.dataVolume = doc.dataVolume ? Number(doc.dataVolume) : 0;
		data.operationCount = doc.operationCount ? Number(doc.operationCount) : 1;
		var timestamp = Number(doc.creationTime);
                var date = new Date(timestamp);
		var dataKey = [];
		dataKey.push(date.getUTCFullYear());
		dataKey.push(date.getUTCMonth()+1);
		dataKey.push(date.getUTCDate());
		dataKey.push(date.getUTCHours());
		dataKey.push(date.getUTCMinutes());
	  
		
		emit(dataKey, data);
	}
}	
 
