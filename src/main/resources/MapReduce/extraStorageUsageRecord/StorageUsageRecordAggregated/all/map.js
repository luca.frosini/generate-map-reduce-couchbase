function(doc,meta) {
	if(doc.usageRecordType == "StorageUsageRecord" || doc.recordType == "StorageUsageRecord"){
		if((doc.duration && isNaN(Number(doc.duration))) || (doc.operationCount && isNaN(Number(doc.operationCount)))){
			emit([0, "Invalid Value", doc.id], doc);
			return;
		}
 		var data = {};
		data._id = doc._id;
		data._rev = doc._rev;
		data.aggregated = doc.aggregated;
		data.consumerId=doc.consumerId;          
		data.creationTime = doc.creationTime;
		data.dataType = doc.dataType;          
		data.dataVolume = doc.dataVolume ? Number(doc.dataVolume) : 0;          
		data.endTime = doc.endTime;        
		data.id = doc.id;
		data.operationCount = doc.operationCount ? Number(doc.operationCount) : 1;         
		data.operationResult = doc.operationResult;      
		data.operationType = doc.operationType;  
		data.providerURI = doc.providerURI;            
		data.resourceOwner = doc.resourceOwner;  
		data.resourceScope = doc.resourceScope;  
		data.resourceURI = doc.resourceURI;  
		data.scope = doc.scope;  
		data.startTime = doc.startTime ? Number(doc.startTime) : doc.creationTime;
		data.usageRecordType = doc.usageRecordType;  
		data.recordType=doc.recordType;          
		var timestamp = Number(doc.creationTime);
		var date = new Date(timestamp);
		var dataKey = [];
		dataKey.push(date.getUTCFullYear());
		dataKey.push(date.getUTCMonth()+1);
		dataKey.push(date.getUTCDate());
		dataKey.push(date.getUTCHours());
		dataKey.push(date.getUTCMinutes());
		emit(dataKey, data);
	}
}
