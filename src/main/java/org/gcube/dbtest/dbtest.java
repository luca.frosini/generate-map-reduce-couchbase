package org.gcube.dbtest;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.bucket.BucketManager;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import com.couchbase.client.java.error.DesignDocumentAlreadyExistsException;
import com.couchbase.client.java.view.DefaultView;
import com.couchbase.client.java.view.DesignDocument;
import com.couchbase.client.java.view.View;

public class dbtest {

	protected static final String DB = "AggregatedServiceUsageRecord";
	protected static final String USAGE_RECORD_TYPE = "ServiceUsageRecord";
	
	protected static final String URL = "http://couchbase01-d-d4s.d4science.org";
	protected static final String PASSWORD = "accounting";

	/* The environment configuration */
	protected static final CouchbaseEnvironment ENV = DefaultCouchbaseEnvironment.builder().queryEnabled(true).build();

	public static void main(String[] args) throws Exception {
		createMapReduceCouchBase();
		createExtraMapReduceCouchBase();
	}

	protected static void createMapReduceCouchBase() throws Exception {

		Cluster cluster = CouchbaseCluster.create(ENV, URL);
		Bucket bucket = cluster.openBucket(DB, PASSWORD);
		BucketManager bucketManager = bucket.bucketManager();
		
		File currentDirectory = new File(".");
		File srcDirectory = new File(currentDirectory, "src");
		File mainDirectory = new File(srcDirectory, "main");
		File resourcesDirectory = new File(mainDirectory, "resources");
		File mapReduceDirectory = new File(resourcesDirectory, "MapReduce");
		
		File file = new File(mapReduceDirectory, USAGE_RECORD_TYPE);
		
		File[] filesInDir = file.listFiles();
		Arrays.sort(filesInDir);
		Map<String, List<View>> views = new HashMap<String, List<View>>();

		for (File fs : filesInDir) {
			
			String viewName = fs.getName().split("__")[0];
			String mapReduceName = fs.getName();

			File mapFile = new File(fs, "map.js");
			File reduceFile = new File(fs, "reduce.js");

			String mapContent = new String(Files.readAllBytes(Paths.get(mapFile.toURI())));
			String reduceContent = new String(Files.readAllBytes(Paths.get(reduceFile.toURI())));
			View view = DefaultView.create(mapReduceName, mapContent, reduceContent);
			
			if (!views.containsKey(viewName)){
				views.put(viewName, new ArrayList<View>());
			}
			views.get(viewName).add(view);
		}

		for (Map.Entry<String, List<View>> entry : views.entrySet()) {
			String viewName = entry.getKey();
			DesignDocument designDoc = DesignDocument.create(viewName, entry.getValue());
			bucketManager.insertDesignDocument(designDoc);
		}

		cluster.disconnect();
	}

	
	protected static void createExtraMapReduceCouchBase() throws Exception {

		Cluster cluster = CouchbaseCluster.create(ENV, URL);
		Bucket bucket = cluster.openBucket(DB, PASSWORD);
		BucketManager bucketManager = bucket.bucketManager();
		File currentDirectory = new File(".");
		File srcDirectory = new File(currentDirectory, "src");
		File mainDirectory = new File(srcDirectory, "main");
		File resourcesDirectory = new File(mainDirectory, "resources");
		File mapReduceDirectory = new File(resourcesDirectory, "MapReduce");
		File file = new File(mapReduceDirectory, "extra" + USAGE_RECORD_TYPE);
		
		Map<String, List<View>> views = new HashMap<String, List<View>>();

		for (File f : file.listFiles()){
			
			String viewName = f.getName();
			if (!views.containsKey(viewName)){
				views.put(viewName, new ArrayList<View>());
			}
			
			for(File fs : f.listFiles()){
				String mapReduceName = fs.getName();
	
				File mapFile = new File(fs, "map.js");
				File reduceFile = new File(fs, "reduce.js");
	
				String mapContent = new String(Files.readAllBytes(Paths.get(mapFile.toURI())));
				String reduceContent = new String(Files.readAllBytes(Paths.get(reduceFile.toURI())));
				
				View view = DefaultView.create(mapReduceName, mapContent, reduceContent);
				
				views.get(viewName).add(view);
			}
		}

		for (Map.Entry<String, List<View>> entry : views.entrySet()) {
			String viewName = entry.getKey();
			DesignDocument designDoc = DesignDocument.create(viewName, entry.getValue());
			try{
				bucketManager.insertDesignDocument(designDoc);
			}catch (DesignDocumentAlreadyExistsException e) {
				System.out.println(viewName + " Already Exists");
			}
		}

		cluster.disconnect();
	}
}
